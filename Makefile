CC=gcc

CFLAGS=-Wall

LIBS=-lpcap

all:main
	
main:main.o m_packet.o flux.o io.o
	$(CC) $(CFLAGS) -o main main.o m_packet.o flux.o io.o $(LIBS)

m_packet:m_packet.o 
	$(CC) $(CFLAGS) -o m_packet m_packet.o $(LIBS)

flux:flux.o m_packet.o
	$(CC) $(CFLAGS) -o flux flux.o m_packet.o $(LIBS)

io:io.o flux.o m_packet.o
	$(CC) $(CFLAGS) -o flux flux.o m_packet.o $(LIBS)

m_packet.o:m_packet.c m_packet.h
flux.o:flux.c flux.h 
io.o:io.c io.h

clean:
	rm -rf *.o
