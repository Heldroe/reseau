// Packet struct
typedef struct m_packet {
    u_long seq;        // Sequence ID
    int data_length;   // Data length of packet
    int dup;           // Indicates if packet is a duplicate
    int delay;         // Delay measured in packet number
} m_packet;

// Linked list of packets struct
typedef struct m_packet_node {
    struct m_packet* m_packet;  // Packet data
    struct m_packet_node* next; // Next packet node
} m_packet_node;

// Adds a packet to specified linked list of packets
m_packet_node* add_packet (m_packet_node** node, m_packet* p);

// Creates a packet structure from speciefied data
m_packet* create_packet (u_long seq, int dup, int delay, int data_length);

// Deletes a packet linked list from memory
void delete_packet_list (m_packet_node* packets);

// Finds a packet structure in specified linked list, corresponding to specified sequence id
m_packet* find_packet (m_packet_node** node, u_long seq);

// Displays all data in packet linked list
void display_packet_list (m_packet_node* node);

// Deletes a packet corresponding to specified sequence id, from specified packet linked list
void delete_packet (m_packet_node** node, u_long seq);

// Increment delay value in all packets contained in specified packet linked list
void increment_delay (m_packet_node** node);

// Counts the number of duplicated packets in specified packet linked list
int dup_in_flux(m_packet_node* node);