#include <stdlib.h>
#include <pcap.h>
#include "m_packet.h"

m_packet_node* add_packet (m_packet_node** node, m_packet* p) {
    m_packet_node* n = malloc(sizeof(m_packet_node));
    m_packet_node* t = *node;

    n->m_packet = p;
    n->next = NULL;

    if (*node != NULL) {
        while (t->next != NULL) t = t->next;
        t->next = n;
    } else {
        *node = n;
    }

    return n;
}

m_packet* create_packet (u_long seq, int dup, int delay, int data_length) {
    m_packet* p = malloc(sizeof(m_packet));
    p->seq = seq;
    p->data_length = data_length;
    p->dup = dup;
    p->delay = delay;
    return p;
}

void delete_packet_list (m_packet_node* first) {
    m_packet_node* node = NULL;
    while (first != NULL) {
        node = first;
        first = node->next;
        free(node->m_packet);
        free(node);
    }
}

m_packet* find_packet (m_packet_node** node, u_long seq) {
    m_packet_node* t = *node;
    while (t != NULL) {
        if (t->m_packet->seq == seq) return t->m_packet;
        t = t->next;
    }
    return NULL;
}

void delete_packet (m_packet_node** node, u_long seq) {
    m_packet_node* t = *node;
    m_packet_node* p = NULL; // Previous node
    while (t != NULL) {
        if (t->m_packet->seq == seq) {
            if (p != NULL) p->next = NULL;
            else *node = NULL;
            free(t->m_packet);
        }
        p = t;
        t = t->next;
    }
}

void display_packet_list (m_packet_node* node) {
    while (node != NULL) {
        printf("SEQ=%ld DL=%d\tNSEQ=%ld", node->m_packet->seq, node->m_packet->data_length,
            node->m_packet->seq+node->m_packet->data_length);
        if (node->m_packet->dup) printf(" (DUP)");
        if (node->m_packet->delay) printf(" *D+%d*", node->m_packet->delay);
        printf("\n");
        node = node->next;
    }
}

void increment_delay (m_packet_node** node) {
    m_packet_node* t = *node;
    while (t != NULL) {
        t->m_packet->delay++;
        t = t->next;
    }
}

int dup_in_flux(m_packet_node* node) {
    int sum = 0;
    while (node != NULL) {
        sum += node->m_packet->dup;
        node = node->next;
    }
    return sum;
}