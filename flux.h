// Flux struct: identified by source ip and port, dest ip and port
// Incoming and outgoing packets are using different flux structs
typedef struct flux {
    uint32_t src_ip; // Source IP
    uint32_t dst_ip; // Destination IP
    u_int  src_port; // Source port
    u_int  dst_port; // Destination port
    m_packet_node* packets;   // All packets
    m_packet_node* m_packets; // Missing packets
} flux;

// Linked list structure
typedef struct flux_node {
    struct flux* flux;      // Flux data
    struct flux_node *next; // Next node
} flux_node;

// Add a flux to a linked list of flux
flux_node* add_flux (flux_node** node, flux* f);

// Delete a linked list of flux from memory
void delete_flux_list (flux_node* flux);

// Create a new flux from specified data
flux* create_flux (uint32_t src_ip, uint32_t dst_ip, u_int src_port, u_int dst_port);

// Find a flux in a linked list of flux, corresponding to specified data
flux* find_flux (flux_node** node, uint32_t src_ip, uint32_t dst_ip, u_int src_port, u_int dst_port);

// Find or creates a flux in a linked list, corresponding to specified data (uses find_flux and create_flux)
flux* find_or_create_flux (flux_node** node, uint32_t src_ip, uint32_t dst_ip, u_int src_port, u_int dst_port);

// Displays all data in specified linked list of flux
void display_flux_list (flux_node* node);

// Translates an integer IP into a human-readable string
void ip_to_str (char** buf, int ip);

// Counts packets duplicated in specified flux linked list
int duplicate_count (flux_node* node);