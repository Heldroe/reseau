#define _BSD_SOURCE

#include <stdio.h>
#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <byteswap.h>
#include <string.h>

#include "m_packet.h"
#include "flux.h"
#include "io.h"

// Packet reading function
void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet);

flux_node* flux_list; // List of flux

int main (int argc, char** argv) {

    char errbuf[PCAP_ERRBUF_SIZE]; // Error buffer
    pcap_t* file;                  // File descriptor

    if (argc > 1) { // At least 1 argument (file name)

        file = pcap_open_offline(argv[1], errbuf); // Opens specified file

        if (file == NULL) { // Error opening file
            printf("Invalid file.\n");
            return 1;
        }

        if (pcap_loop(file, 0, packetHandler, NULL) < 0) { // Loops all packets in file
            printf("Failed to read file.\n"); // Error reading file
            return 1;
        }   

        pcap_close(file); // Close file as soon as possible

        if (argc == 3 && strcmp(argv[2], "-v") == 0) {        // Verbose mode
            display_flux_list(flux_list);                     // Verbose display
        } else if (argc == 3 && strcmp(argv[2], "-c") == 0) { // Duplicate count mode
            printf("%d duplicates found.\n", duplicate_count(flux_list)); // Display duplicates
        } else {                                              // Standard (CSV output) mode
            print_flux_list(flux_list);                       // CSV output
        }

        delete_flux_list(flux_list); // Delete list from memory

        return 0;

    }

    printf("You must specify a pcap file to read.\n");
    return 1;
}

void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet) {
    const struct ether_header* eth; // Ethernet header
    const struct ip*            ip; // IP header
    const struct tcphdr*       tcp; // TCP header

    u_int src_port, dst_port; // Source and destination ports
    int dataLength = 0;       // Packet data length
    int dup = 0;              // Duplicate
    int delay = 0;            // Packet delay
    u_long seq;               // Sequence id
    flux* f;                  // Flux structure
    m_packet *p, *m_p;        // Current and missing (next expected) packet
    m_packet *e_p;             // Expected packet
 
    eth = (struct ether_header*)packet;                          // Fill ethernet structure
    if (ntohs(eth->ether_type) == ETHERTYPE_IP) {                // Only read IP protocol
        ip = (struct ip*)(packet + sizeof(struct ether_header)); // Fill IP structure

        if (ip->ip_p == IPPROTO_TCP) {                                                        // Only read TCP/IP protocol
            tcp = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip)); // Fill TCP structure
            src_port = ntohs(tcp->th_sport); // Source port
            dst_port = ntohs(tcp->th_dport); // Destination port
            dataLength = pkthdr->len - (sizeof(struct ether_header) + sizeof(struct ip) + sizeof(struct tcphdr) + 12); // Data length
            seq = (u_long) __bswap_32(tcp->th_seq); // Sequence ID

            if (tcp->th_flags & TH_PUSH) { // Filter on PSH flag only
                // Finds or create flux corresponding to packet
                f = find_or_create_flux(&flux_list, ip->ip_src.s_addr, ip->ip_dst.s_addr, src_port, dst_port);
                if (find_packet(&(f->packets), seq)) dup = 1; // Packet is a duplicate if already exists

                e_p = find_packet(&(f->m_packets), seq); // Is this packet expected?
                if (e_p != NULL) {                       // If expected
                    delay = e_p->delay-1;                // Get delay 
                    delete_packet(&(f->m_packets), seq); // Delete from missing packet list
                }

                p = create_packet(seq, dup, delay, dataLength); // Create packet structure
                add_packet(&(f->packets), p);                   // Add packet structure to flux
                
                if (!dup) {                                       // If packet is not a duplicate
                    m_p = create_packet(seq+dataLength, 0, 0, 0); // Create a packet structure with sequence ID
                    add_packet(&(f->m_packets), m_p);             // Add packet structure to missing packets in flux (expected packets)
                }

                increment_delay(&(f->m_packets)); // Increment delay in all missing packets in flux
            }
        }
    }
}
