#include <stdio.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <pcap.h>

#include "m_packet.h"
#include "flux.h"
#include "io.h"

void print_flux (flux* f) {
    m_packet_node* p = f->packets;
    while (p != NULL) {
        printf("%ld,%d,%d\n", p->m_packet->seq, p->m_packet->delay, p->m_packet->delay >= 3);
        p = p->next;
    }
}

void print_flux_list (flux_node* node) {
    printf("packet_id,delay,retransmitted\n");
    while (node != NULL) {
        print_flux(node->flux);
        node = node->next;
    }
}