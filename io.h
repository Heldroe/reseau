// Prints CSV data from specified flux
void print_flux (flux* f);

// Prints CSV data from specified flux linked list
void print_flux_list (flux_node* node);