#Projet Internet et Réseaux
##Compilation
* Compiler avec `make`
* Nettoyer avec `make clean`
##Exécution
###Syntaxe
`./main [pcap file] [argument]`
###Arguments
* `-c` affiche le nombre de packets dupliqués dans la trace *(**c**ount)*
* `-v` affiche les informations dans un format plus lisible *(**v**erbose)*
* Sans argument, affiche les informations au format CSV demandé
* Un seul argument peut être utilisé à la fois `-c` ou `-v` mais pas les deux en même temps
###Fichier PCAP
* Le fichier en argument doit être au format pcap
###Exemples
* `./main test.pcap`
* `./main test.pcap -v`
* `./main test.pcap -c`
* `./main test.pcap > output.csv`