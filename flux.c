#include <stdlib.h>
#include <netinet/ip.h>
#include <pcap.h>

#include "m_packet.h"
#include "flux.h"

flux_node* add_flux (flux_node** node, flux* f) {
    flux_node* n = malloc(sizeof(flux_node));
    flux_node* t = *node;

    n->flux = f;
    n->next = NULL;

    if (*node != NULL) {
        while (t->next != NULL) t = t->next;
        t->next = n;
    } else {
        *node = n;
    }

    return n;
}

void delete_flux_list (flux_node* first) {
    flux_node* node = NULL;
    while (first != NULL) {
        node = first;
        delete_packet_list(node->flux->m_packets);
        first = node->next;
        free(node->flux);
        free(node);
    }
}

flux* create_flux (uint32_t src_ip, uint32_t dst_ip, u_int src_port, u_int dst_port) {
    flux* f = malloc(sizeof(flux));
    f->src_ip    = src_ip;
    f->dst_ip    = dst_ip;
    f->src_port  = src_port;
    f->dst_port  = dst_port;
    f->m_packets = NULL;
    f->packets   = NULL;

    return f;
}

flux* find_flux (flux_node** node, uint32_t src_ip, uint32_t dst_ip, u_int src_port, u_int dst_port) {
    flux_node* t = *node;
    while (t != NULL) {
        if (t->flux->src_ip      == src_ip
            && t->flux->dst_ip   == dst_ip
            && t->flux->src_port == src_port
            && t->flux->dst_port == dst_port)
            return t->flux;
        t = t->next;
    }
    return NULL;
}

flux* find_or_create_flux (flux_node** node, uint32_t src_ip, uint32_t dst_ip, u_int src_port, u_int dst_port) {
    flux* r = find_flux(node, src_ip, dst_ip, src_port, dst_port);
    if (r == NULL) {
        r = create_flux(src_ip, dst_ip, src_port, dst_port);
        add_flux(node, r);
    }
    return r;
}

void display_flux_list (flux_node* node) {
    char* src_ip = malloc(sizeof(char) * INET_ADDRSTRLEN + 1);
    char* dst_ip = malloc(sizeof(char) * INET_ADDRSTRLEN + 1);
    
    while (node != NULL) {
        ip_to_str(&src_ip, node->flux->src_ip);
        ip_to_str(&dst_ip, node->flux->dst_ip);
        printf("%s:%d -> %s:%d\n", src_ip, node->flux->src_port, dst_ip, node->flux->dst_port);
        display_packet_list(node->flux->packets);
        printf("\n");
        node = node->next;
    }

    free(src_ip);
    free(dst_ip);
}

void ip_to_str (char** buf, int ip) {
    unsigned char bytes[4];
    bytes[0] = ip & 0xFF;
    bytes[1] = (ip >> 8) & 0xFF;
    bytes[2] = (ip >> 16) & 0xFF;
    bytes[3] = (ip >> 24) & 0xFF;   
    sprintf(*buf, "%d.%d.%d.%d", bytes[0], bytes[1], bytes[2], bytes[3]);
}

int duplicate_count (flux_node* node) {
    int sum = 0;
    while (node != NULL) {
        sum += dup_in_flux(node->flux->packets);
        node = node->next;
    }
    return sum;
}